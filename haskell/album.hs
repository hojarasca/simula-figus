import System.Random
import Data.List(union, nub, findIndex)
import Data.Maybe (fromJust)


sobre :: ([Int],StdGen) -> ([Int],StdGen)
sobre (a,b) = if (length a == 5) then (nub a,b') else sobre (a ++ [figu], b')
	where (figu,b') = randomR (1,500) b

sobres :: StdGen -> [[Int]]
sobres g = a : sobres g'
  where (a,g') = sobre ([],g)

llenar :: [[Int]] -> [[Int]]
llenar = (scanl1 union)


p = sobres (mkStdGen 34876456)


simular xs = fromJust (findIndex (\l -> (length l)>=500) xs)

ejercicio16 :: Int -> [[Int]] -> [Int] -> Float
ejercicio16 num fss casos
  | length casos == num = acumulado / n
  | length casos < num = ejercicio16 num (drop ind fss) (ind:casos)
 where ind = simular (llenar fss)
       n = fromInteger (toInteger num) :: Float
       acumulado = fromInteger (toInteger(sum casos)) :: Float



main = do
  gen <- getStdGen
  let numero = 1000
      a = ejercicio16 numero (sobres gen) []
  putStrLn(show a)
