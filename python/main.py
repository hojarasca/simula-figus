from simulacion import Simulacion
import time

simulacion = Simulacion()

total = 0
for _ in range(1000):
    total += simulacion.correr()

print(total)
