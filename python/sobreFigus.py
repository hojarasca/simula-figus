import random

class SobreFigus:
    def __init__(self):
        self.figuritas = [ random.randrange(1, 501) for _ in range(5) ]
    
    # Devuelve las figuritas del sobre. Un mismo sobre se puede "abrir" muchas veces, pero siempre devuelve lo mismo.
    def abrir(self):
        return self.figuritas
