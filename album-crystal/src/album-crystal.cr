class Album
  def initialize()
    @estado = Set(Int32).new
  end

  def pegar(figu : Int32)
    @estado.add(figu)
  end

  def agregar_sobre(sobre : Sobre)
    sobre.abrir.each do |figu|
      self.pegar(figu)
    end
  end

  def esta_completo?
    @estado.size >= 500
  end
end


class Sobre
  @figus : Array(Int32)

  def initialize()
    @figus = (1..5).map{|_| Random.rand 1..501}
  end

  def abrir()
    return @figus
  end
end

class Simulacion
  def completarAlbum()
    album = Album.new
    a = 0
    while !album.esta_completo?
      album.agregar_sobre(Sobre.new)
      a+=1
    end
    a
  end
end

total = 0
1000.times do |a|
  total += Simulacion.new.completarAlbum()
end

puts total
